# README #

This README would normally document whatever steps are necessary to get your application up and running. But it doesn't.

### What is this repository for? ###

This is source code for a small site intended for educational use, written by me for me. Site can be accessed at http://edu.lattenwald.org.

### Features ###

* simple mathematical problems generator (check out http://edu.lattenwald.org)
* persistent problems list (you will be redirected to some URL with specific RNG salt, and can re-visit this URL to view this exact set of problems again)
* solutions provided (just add /solutions to the end of URL)

### Technology used ###

* [Spock](https://hackage.haskell.org/package/Spock)
* Some other stuff, check edu.cabal

### How do I get set up? ###

I should fill this section sometime later.

### Who do I talk to? ###

* Me: qalexx, using gmail.com