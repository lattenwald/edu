data Expr = VInt Int | Add Expr Expr | Subtract Expr Expr | Multiply Expr Expr
  deriving (Show, Eq)
