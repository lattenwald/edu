module Expr where

import Prelude hiding (signum, negate)
import qualified Prelude as P

import Control.Monad.Random

data Expr = Value Int | Add Expr Expr | Multiply Expr Expr
  deriving (Show, Eq)

reduce e@(Value _) = e
reduce (Add e1 e2) = Value (v1 + v2)
  where Value v1 = reduce e1
        Value v2 = reduce e2
reduce (Multiply e1 e2) = Value (v1 * v2)
  where Value v1 = reduce e1
        Value v2 = reduce e2

signum e = P.signum v
  where Value v = reduce e

negate (Value v) = Value (-v)
negate (Add e1 e2) = Add (negate e1) (negate e2)
negate (Multiply e1 e2) = Multiply (negate e1) e2

showExpr (Value v) = show v
showExpr (Add e1 e2) = showExpr e1 ++ op ++ showExpr e2'
  where (op, e2') = if signum e2 < 0
                       then (" - ", negate e2)
                       else (" + ", e2)

lowLimit = -20
upLimit = 50

genSimpleA :: MonadRandom m => m Expr
genSimpleA = do
  x <- getRandomR (1, upLimit)
  y <- getRandomR (x+lowLimit, upLimit)
  return $ Add (Value x) (Value y)

genSimpleA2 :: MonadRandom m => m Expr
genSimpleA2 = do
  e <- genSimpleA
  let (Value x) = reduce e
  y <- getRandomR (x+lowLimit, upLimit)
  return $ Add e (Value y)
