{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
import           Control.Monad.Random
import           Control.Monad.Trans.Class
import           Web.Spock.Safe
import qualified Data.Text as T
import           Data.List (intercalate)
import           Network.Wai
import           Network.Wai.Handler.Warp

import qualified Legacy
import           Expr

exercisesR :: Path '[Int]
exercisesR = "ex1" <//> var

solutionsR :: Path '[Int]
solutionsR = exercisesR <//> "solution"

stuff :: SpockT IO ()
stuff = do
  get exercisesR $ \seed -> out seed (map showEx $ exercises seed)
  get solutionsR $ \seed -> out seed (map showSol $ exercises seed)
  where
    showEx e = showExpr e ++ " ="
    showSol e = showEx e ++ " " ++ showExpr (reduce e)

    out seed stuff = text $ T.concat [
      renderRoute exercisesR seed
      , "\n\n"
      , T.pack . intercalate "\n" $ stuff ]

    exercises :: Int -> [Expr]
    exercises seed = flip evalRand (mkStdGen seed) $ do
      let e1 = replicate 40 genSimpleA
          e2 = replicate 20 genSimpleA2
      sequence (e1 ++ e2)

index :: ActionT IO ()
index = do
  g <- lift newStdGen
  let (seed, _) = random g :: (Int, _)
  redirect $ renderRoute exercisesR seed

app :: IO Middleware
app = spockT id $ do
  Legacy.stuff
  stuff
  get root index

main :: IO ()
main =
  runSettings settings =<< spockAsApp app
  -- run 3000 (spockAsApp app)
  -- runSpock 3000 app
  where
    settings = setPort 8165 . setHost "127.0.0.1" $ defaultSettings
