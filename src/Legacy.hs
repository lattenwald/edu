{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
module Legacy where

import           Data.List (intercalate)
import qualified Data.Text as T
import           Control.Monad.Random
import           Web.Spock.Safe
import qualified Expr as E

exercisesR :: Path '[Int]
exercisesR = "ex" <//> var

solutionsR :: Path '[Int]
solutionsR = exercisesR <//> "solution"

math_exercise :: SpockT IO ()
math_exercise = get exercisesR $
  \seed -> text
           $ T.concat [
             renderRoute exercisesR seed
             , "\n\n"
             , T.pack
               . intercalate "\n"
               . map (++ " =")
               . map E.showExpr
               $ exercises seed ]

math_exercise_solutions :: SpockT IO ()
math_exercise_solutions = get solutionsR $
  \seed -> text
           $ T.concat [
             renderRoute solutionsR seed
             , "\n\n"
             , T.pack
               . intercalate "\n"
               . map (\e -> E.showExpr e ++ " = " ++ E.showExpr (E.reduce e))
               $ exercises seed ]

exercises :: Int -> [E.Expr]
exercises seed = flip evalRand (mkStdGen seed) $ do
  let e1 = replicate 40 genSimpleA
      e2 = replicate 20 genSimpleA2
  sequence (e1 ++ e2)

lowLimit = -20
upLimit = 50

genSimpleA :: MonadRandom m => m E.Expr
genSimpleA = do
  x <- getRandomR (1, upLimit)
  y <- getRandomR (x-lowLimit, upLimit)
  return $ E.Add (E.Value x) (E.Value y)

genSimpleA2 :: MonadRandom m => m E.Expr
genSimpleA2 = do
  e <- genSimpleA
  let (E.Value x) = E.reduce e
  y <- getRandomR (x-lowLimit, upLimit)
  return $ E.Add e (E.Value y)

stuff :: SpockT IO ()
stuff = do
  math_exercise
  math_exercise_solutions
